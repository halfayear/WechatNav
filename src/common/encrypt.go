package common

import (
	"crypto/md5"
	"encoding/hex"

)

func Md5(src string)  string {

	md5Ctx := md5.New()
	md5Ctx.Write([]byte(src))
	cipherStr := md5Ctx.Sum(nil)
	 return hex.EncodeToString(cipherStr)
}
