/**
 * Created by zchuanzhao on 2016/12/6.
 */
$.ajax({
    url: basepath+"/area/provinceList",
    type: "get",
    dataType: "json",
    async : true,
    timeout: 10000,
    success:function(res){
        var option = "";
        $.each(res.provinceList, function(k, p) {
            console.log(p.name);
            option += "<option value='" + p.name + "' data-code='"+p.code+"'>" + p.name + "</option>";
        });
        $("#province").append(option);
    }
});

$("#province").change(function () {
    var code = $(this).find("option:selected").attr("data-code");
    $("#city option:gt(0)").remove();
    $.ajax({
        url: basepath+"/area/cityList/"+code,
        type: "get",
        dataType: "json",
        async : true,
        timeout: 10000,
        success:function(res){
            var option = "";
            $.each(res.cityList, function(k, p) {
                option += "<option value='" + p.name + "' data-code='"+p.code+"'>" + p.name + "</option>";
            });
            $("#city").append(option);
        }
    });
});